import Head from "next/head";
import Link from "next/link";
import { CardanoWallet, useWallet } from "@meshsdk/react";
import { useState } from "react";
import { Box, Heading, Container, Text, Button, Stack, Icon, Link as CLink, createIcon, Grid, Divider } from "@chakra-ui/react";

export default function CallToActionWithAnnotation() {
  const { connected, wallet } = useWallet();
  const [assets, setAssets] = useState<null | any>(null);
  const [loading, setLoading] = useState<boolean>(false);
  return (
    <>
      <Container maxW={["100%", "90%"]} pb="40">
        <Stack as={Box} spacing={{ base: 10, md: 5 }} py="15">
          <Box w="60%" py="20">
            <Stack direction={"column"} position={"relative"}>
              <Heading fontWeight={600} fontSize={{ base: "2xl", sm: "2xl", md: "6xl" }} lineHeight={"150%"}>
                Plutus Project-Based Learning <br />
                <Text as={"span"} color={"theme.green"}>
                  from Gimbalabs
                </Text>
              </Heading>
            </Stack>
            <Text fontSize={["lg", "xl"]} py="5">
              Gimbalabsでは、分散型エコシステムが教育に対する人々の考え方を変えると信じています。
              また、分散型エコシステムが繁栄するためには、行動指向型で学生中心の教育が必要であると考えています。
            </Text>
            <Text fontSize={["lg", "xl"]} py="5">
              {" "}
              <Text as="span" fontWeight="900" color="theme.green">
                Project-Based Learning Course
              </Text>{" "}
              の目標は、あなたのような方々が実際の{" "}
              <Text as="span" fontWeight="900" color="theme.green">
                プロジェクト
              </Text>{" "}
              に{" "}
              <Text as="span" fontWeight="900" color="theme.green">
                貢献者
              </Text>
              として参加できるようサポートすることです。
            </Text>
          </Box>
          <Grid templateColumns='repeat(2, 1fr)' gap={10}>
            <Box border="1px" p="5" borderRadius="xl">
              <Heading fontWeight={600} fontSize="2xl" lineHeight={"110%"}>
                コース状況
              </Heading>
              <Text fontSize="lg" py="3">2023年版のPlutus PBLは現在終了しました。</Text>
              <Text fontSize="lg" py="3">更新された&quot;PPBL 2024&quot;コースは、2024年初頭に開始されます。</Text>
              <Text fontSize="lg" py="3">
                もしCardano開発が初めての方であれば、いつでもPlutus PBL 2023コースを閲覧することができます。
              </Text>

              <Stack direction={"row"} spacing={10} align={"center"} alignSelf={"center"} position={"relative"} my="10">
                <Button
                  bg={"theme.cyan"}
				  textColor="theme.dark"
                  rounded={"full"}
                  px={6}
                  _hover={{
                    bg: "theme.green",
                  }}
                >
                  <Link href="/get-started">PPBL 2023コースを閲覧する</Link>
                </Button>
                <Button
                  bg={"theme.yellow"}
				  textColor="theme.dark"
                  rounded={"full"}
                  px={6}
                  _hover={{
                    bg: "theme.green",
                  }}
                >
                  <Link href="/live-coding">Live Codingのアーカイブを閲覧する</Link>
                </Button>
              </Stack>
            </Box>
            <Box border="1px" p="5" borderRadius="xl">
              <Heading fontWeight={600}  fontSize="2xl"  lineHeight={"110%"}>
                次は何ですか？
              </Heading>
			  <Text fontSize="lg" py="3">
                2024年1月、Live Coding sessionsは「course governance」に焦点を当てます。{" "}
                <CLink href="https://sociocracy30.org/">Sociocracy 3.0</CLink> 
                のパターンを利用して、教育が分散型エコシステムをサポートする役割を探求したい人々に、そのコースの所有権を分配します。
                </Text>
			  <Text fontSize="lg" py="3">
               2024年1月10日水曜日、UTC 1430時に、{" "}
                <CLink href="https://patterns.sociocracy30.org/driver-mapping.html">
                  first Driver Mapping Workshop
                </CLink>
                に皆様をご招待いたします。ここでは、GimbalabsにおけるPPBLの将来の計画を立てます。どなたでも参加可能です。
              </Text>
              <Stack direction={"row"} spacing={10} align={"center"} alignSelf={"center"} position={"relative"} my="10" mx="auto">
                <Button
                  bg={"theme.green"}
				  textColor="theme.dark"
                  rounded={"full"}
                  px={6}
				  mx="auto"
                  _hover={{
                    bg: "theme.yellow",
                  }}
                >
                  <Link href="/governance">「course governance」に参加する</Link>
                </Button>
              </Stack>
            </Box>
        </Grid>
          </Stack>
      </Container>
    </>
  );
}

// const Arrow = createIcon({
//   displayName: "Arrow",
//   viewBox: "0 0 72 24",
//   path: (
//     <path
//       fillRule="evenodd"
//       clipRule="evenodd"
//       d="M0.600904 7.08166C0.764293 6.8879 1.01492 6.79004 1.26654 6.82177C2.83216 7.01918 5.20326 7.24581 7.54543 7.23964C9.92491 7.23338 12.1351 6.98464 13.4704 6.32142C13.84 6.13785 14.2885 6.28805 14.4722 6.65692C14.6559 7.02578 14.5052 7.47362 14.1356 7.6572C12.4625 8.48822 9.94063 8.72541 7.54852 8.7317C5.67514 8.73663 3.79547 8.5985 2.29921 8.44247C2.80955 9.59638 3.50943 10.6396 4.24665 11.7384C4.39435 11.9585 4.54354 12.1809 4.69301 12.4068C5.79543 14.0733 6.88128 15.8995 7.1179 18.2636C7.15893 18.6735 6.85928 19.0393 6.4486 19.0805C6.03792 19.1217 5.67174 18.8227 5.6307 18.4128C5.43271 16.4346 4.52957 14.868 3.4457 13.2296C3.3058 13.0181 3.16221 12.8046 3.01684 12.5885C2.05899 11.1646 1.02372 9.62564 0.457909 7.78069C0.383671 7.53862 0.437515 7.27541 0.600904 7.08166ZM5.52039 10.2248C5.77662 9.90161 6.24663 9.84687 6.57018 10.1025C16.4834 17.9344 29.9158 22.4064 42.0781 21.4773C54.1988 20.5514 65.0339 14.2748 69.9746 0.584299C70.1145 0.196597 70.5427 -0.0046455 70.931 0.134813C71.3193 0.274276 71.5206 0.70162 71.3807 1.08932C66.2105 15.4159 54.8056 22.0014 42.1913 22.965C29.6185 23.9254 15.8207 19.3142 5.64226 11.2727C5.31871 11.0171 5.26415 10.5479 5.52039 10.2248Z"
//       fill="currentColor"
//     />
//   ),
// });
