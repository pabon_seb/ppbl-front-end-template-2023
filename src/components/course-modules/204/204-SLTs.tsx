import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Divider, Heading, Box, Center, Grid, Text, Link as CLink } from "@chakra-ui/react";
import React from "react";
import Introduction from "@/src/components/course-modules/204/Introduction.mdx";
import LessonNavigation from "@/src/components/lms/Lesson/LessonNavigation";
import VideoComponent from "../../lms/Lesson/VideoComponent";
import LiteYouTubeEmbed from "react-lite-youtube-embed";
import Link from "next/link";

const SLTs204 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <Grid templateColumns="repeat(2, 1fr)" gap={5}>
        <SLTsItems moduleTitle="Module 204" moduleNumber={204} />
        <Box>
          <VideoComponent videoId="esCBPAAJFTQ">About This Module</VideoComponent>
        </Box>
      </Grid>
      <Divider mt="5" />
      <Box w="60%" mx="auto">
        <Text py="5" fontSize="xl">
          このプロジェクトは、これまでにこのコースで学んだ最も重要なスキルを適用する機会です。 
          それを完了することで、あなたは300レベルのモジュールで紹介される開発プロジェクトに貢献できる準備ができていることを示します。
          それを行うと、Cardano開発スタックの異なる部分に特化し始めます。          
        </Text>
        <Text py="5" fontSize="xl">
          このプロジェクトを完了する際に最もワクワクすることを考えてみましょう。私たちが継続的に構築を進める際に焦点を当てたいことは何ですか？
        </Text>
        
        <Text py="5" fontSize="xl">
          {" "}
          <Link href="/live-coding">
            <CLink>Live Coding</CLink>
          </Link>{" "}
          2023-08-02 および 2023-08-03 では、プロジェクトにより詳細なアプローチを取りました。ビデオをこちらで確認できます：
        </Text>
        <Heading size="lg" py="5">
          Live Coding - 2023-08-02
        </Heading>
        <LiteYouTubeEmbed id="NhBIIpZBy4s" title="example video" />
        <Heading size="lg" py="5">
          Live Coding - 2023-08-03
        </Heading>
        <LiteYouTubeEmbed id="i2uyLZq4REQ" title="example video" />
      </Box>
      <Box py="5">
        <Introduction />
      </Box>
      <LessonNavigation moduleNumber={204} currentSlug="slts" />
    </Box>
  );
};

export default SLTs204;
