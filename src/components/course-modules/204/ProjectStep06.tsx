import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep06 from "@/src/components/course-modules/204/DocsStep06.mdx";

export default function ProjectStep05() {
  const slug = "project-step-06";
  const title = "ステップ6: PPBLコースリポジトリにCBORを追加する"

  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="103.1" slug={slug}>
      <DocsStep06 />
    </ProjectLayout>
  );
}
