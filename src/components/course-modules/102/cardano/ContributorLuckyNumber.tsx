import { ConnectWalletMessage } from "@/src/components/ui/Text/ConnectWalletMessage";
import { Box, Center, Flex, Grid, Heading, Spacer, Stack, Text } from "@chakra-ui/react";
import { useWallet } from "@meshsdk/react";

type Props = {
  masteryStatus: {
    ppblTokenName: string;
    cliWallet: string;
    successTx1: boolean;
    successTx2: boolean;
    luckyNumber: number;
  };
};

const ContributorLuckyNumber: React.FC<Props> = ({ masteryStatus }) => {
  const { connected } = useWallet();

  return (
    <Box my="3" bg="theme.lightGray">
      <Box p="3" bg="theme.yellow" color="theme.dark">あなたがブラウザウォレットを接続し、lucky numberが5とは異なることを確認できれば、成功したと言えます。</Box>
      <Box p="3">
      <Flex direction={["column", "row"]}>
        <Center p="5">
          <Heading color="theme.yellow">マスタリー確認</Heading>
        </Center>
        <Spacer />
        <Box w="50%" py="3">
          {masteryStatus.luckyNumber == 5 ? (
            <Text pb="3" fontSize="2xl">
              SLT 102.4および102.5のマスタリーを示すために、上記のマスタリー課題を完了してください。
            </Text>
          ) : (
            <Text pb="3" fontSize="2xl">
              lucky numberを変更できたこと、おめでとうございます！Live Codingにおいて、それがSLT 102.4と102.5を習得したことを十分に「証明」するかどうかについて議論します。
            </Text>
          )}
        </Box>
      </Flex>
      {connected ? (
        <>
          <Grid templateColumns="repeat(2, 1fr)" gap={5} color="theme.dark">
            <Flex w="100%" direction={["column", "row"]}>
              <Center w={["100%", "50%"]} bg="theme.light">
                <Heading size="md">Contributor Token Name:</Heading>
              </Center>
              {masteryStatus.ppblTokenName.length > 0 ? (
                <Center w={["100%", "50%"]} bg="theme.green" p="5">
                  <Text fontSize="2xl">{masteryStatus.ppblTokenName}</Text>
                </Center>
              ) : (
                <Center w={["100%", "50%"]} bg="theme.orange" p="5">
                  <Text fontSize="2xl">トークンが見つかりません</Text>
                </Center>
              )}
            </Flex>
            <Flex w="100%" direction={["column", "row"]}>
              <Center w={["100%", "50%"]} bg="theme.light">
                <Heading size="md">Lucky Number:</Heading>
              </Center>
              {masteryStatus.luckyNumber == 5 ? (
                <Center w={["100%", "50%"]} bg="theme.orange" p="5">
                  <Text>
                    lucky numberは{masteryStatus.luckyNumber}です。上記の課題を完了して、変更してみてください。                                      
                  </Text>
                </Center>
              ) : (
                <Center w={["100%", "50%"]} bg="theme.green" p="5">
                  <Text fontSize="6xl">{masteryStatus.luckyNumber}</Text>
                </Center>
              )}
            </Flex>
          </Grid>
        </>
      ) : (
        <ConnectWalletMessage />
      )}
      </Box>
    </Box>
  );
};

export default ContributorLuckyNumber;
