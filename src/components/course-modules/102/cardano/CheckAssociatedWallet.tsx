import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { ConnectWalletMessage } from "@/src/components/ui/Text/ConnectWalletMessage";
import { useLazyQuery } from "@apollo/client";
import { Box, Heading, Center, Spinner, Text, Badge, Flex, Stack, Divider, Spacer } from "@chakra-ui/react";
import { useWallet, useAddress, CardanoWallet } from "@meshsdk/react";
import * as React from "react";
import { useEffect, useState } from "react";

// Possible todo: add loading and error states to context? Learn best approach.
type Props = {
  children?: React.ReactNode;
  masteryStatus: {
    cliWallet: string;
    successTx1: boolean;
    successTx2: boolean;
  };
};

const CheckAssociatedWallet: React.FC<Props> = ({ children, masteryStatus }) => {
  const { connected } = useWallet();
  return (
    <Box my="3" bg="theme.lightGray">
      <Box p="3" bg="theme.yellow" color="theme.dark">
        ブラウザウォレットを接続して、両方のトランザクションが「on-chain」で見つかることが確認できれば、成功したことがわかります。
      </Box>

      <Box borderColor="theme.four" bg="theme.lightGray" p="3" className="demo-component">
        <Badge size="lg">私たちはLive Codingで、2023-04-05にこのコンポーネントについて話し合います。</Badge>
        <Heading size="lg" my="5">
          関連するCLIウォレットを確認するために、ブラウザウォレットを接続してください
        </Heading>
        <Stack>
          {connected ? (
            <>
              <Box mt="3" pt="1" bg="theme.blue" color="theme.dark">
                <Text pl="1" pt="1" fontSize="xl" fontWeight="bold" color="theme.dark">
                  CLIアドレスを取得します。
                </Text>
                {/* {loadingTx1 && <LoadingComponent>Getting Transactions from Browser Wallet</LoadingComponent>}
          {errorTx1 && <ErrorComponent />} */}
                {masteryStatus.cliWallet ? (
                  <Box bg="theme.green" color="theme.dark" p="3">
                    <Text fontSize="sm">あなたのCLIウォレットのアドレスは： {masteryStatus.cliWallet}</Text>
                  </Box>
                ) : (
                  <Box bg="theme.yellow" p="3">
                    <Text fontSize="sm">
                      このブラウザウォレットに関連付けられたCLIウォレットはありません。PPBL2023トークンをCLIアドレスに送信してみてください。
                    </Text>
                  </Box>
                )}
              </Box>
              <Box mt="3" pt="1" bg="theme.blue" color="theme.dark">
                <Text pl="1" pt="1" fontSize="xl" fontWeight="bold" color="theme.dark">
                  CLIアドレスがSplitUTxO Txを送信したかどうかを確認してください。
                </Text>
                {/* {loadingTx3 && <LoadingComponent>Checking CLI Wallet for SplitUTxO Tx</LoadingComponent>}
          {errorTx3 && <ErrorComponent />} */}
                {masteryStatus.successTx1 ? (
                  <Box bg="theme.green" color="theme.dark" p="3">
                    <Text fontSize="sm">成功：あなたのCLIアドレスはSplitUTxOトランザクションを正常に送信しました！</Text>
                  </Box>
                ) : (
                  <Box bg="theme.yellow" p="3">
                    <Text fontSize="sm">まだです。上記のように、Tx #1を構築し、署名し、送信するようにしてください。</Text>
                  </Box>
                )}
              </Box>
              <Box mt="3" pt="1" bg="theme.blue" color="theme.dark">
                <Text pl="1" pt="1" fontSize="xl" fontWeight="bold" color="theme.dark">
                  CLIアドレスがPPBL2023トークンをブラウザアドレスに返送したことを確認してください。
                </Text>
                {/* {loadingTx2 && <LoadingComponent>Getting Transactions from CLI Wallet to Browser Wallet</LoadingComponent>}
          {errorTx2 && <ErrorComponent />} */}
                {masteryStatus.successTx2 ? (
                  <Box bg="theme.green" color="theme.dark" p="3">
                    <Text fontSize="sm">あなたのPPBL2023トークンが接続されたブラウザウォレットに返送されました</Text>
                  </Box>
                ) : (
                  <Box bg="theme.yellow" p="3">
                    <Text fontSize="sm">まだです。上記に示されているように、Tx #2を送信してみてください。</Text>
                  </Box>
                )}
              </Box>
            </>
          ) : (
            <ConnectWalletMessage />
          )}
        </Stack>
      </Box>
    </Box>
  );
};

export default CheckAssociatedWallet;
