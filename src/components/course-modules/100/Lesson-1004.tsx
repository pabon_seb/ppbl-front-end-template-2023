import ContributorPairMintingComponent from "@/src/components/course-modules/100/ContributorMinter/ContributorPairMintingComponent";
import AssignmentComponent from "@/src/components/lms/Lesson/AssignmentComponent";
import SuccessComponent from "@/src/components/lms/Lesson/SuccessComponent";
import SLT from "@/src/components/ui/Text/SLT";
import {
  Box,
  Divider,
  Grid,
  GridItem,
  Heading,
  ListItem,
  OrderedList,
  Text,
  UnorderedList,
  Link as CLink,
} from "@chakra-ui/react";
import { Asset } from "@meshsdk/core";
import { useAssets, useWallet } from "@meshsdk/react";
import { useEffect, useState } from "react";
import Link from "next/link";
import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import VideoComponent from "@/src/components/lms/Lesson/VideoComponent";

export default function Lesson1004() {
  const { connected } = useWallet();
  const walletAssets = useAssets();

  const [connectedPPBL2023Token, setConnectedPPBL2023Token] = useState<Asset | undefined>(undefined);

  useEffect(() => {
    if (walletAssets) {
      const _ppbl2023 = walletAssets.filter(
        (a: Asset) => a.unit.substring(0, 56) == "05cf1f9c1e4cdcb6702ed2c978d55beff5e178b206b4ec7935d5e056"
      );
      setConnectedPPBL2023Token(_ppbl2023[0]);
    }
  }, [walletAssets]);

  return (
    <LessonLayout moduleNumber={100} sltId="100.4" slug="1004" nextModule="101">
      <Grid mx="auto" fontSize="lg" fontWeight="bold" lineHeight="200%" templateColumns="repeat(2, 1fr)" gap={6}>
        <GridItem w="90%" mx="auto" colSpan={[2, 1]}>
          <Text py="3" textColor="theme.yellow">
            2024年1月1日に更新：Plutus PBLコースは現在終了しており、課題の提出は無効になっています。これにより、PPBL Contributor Tokenを作成することはできません。コースの内容は引き続き閲覧し、関連する動画を視聴することができます。2024年3月にコースの新版が開始されます。on-chainの課題提出はその時点で再度可能になります。
          </Text>
          <Text py="3">
            最初のプロジェクトでは、{" "}
            <Text as="span" color="theme.green" fontWeight="bold">
              PPBL Contributor Token
            </Text>
            に焦点を当てます。
          </Text>
          <Text py="3">Contributor tokenについては、お伝えしたいことがたくさんあります：</Text>
          <UnorderedList ml="10">
            <ListItem><CLink href="https://meshjs.dev/" target="_blank">MeshJS</CLink>を使用して、どのようにマイントされるか</ListItem>
            <ListItem><CLink href="https://cips.cardano.org/cips/cip68/" target="_blank">CIP-68</CLink>の実験的な実装として、どのように役立つか</ListItem>
            <ListItem>Plutusを使ってトークンの動作を変更する方法</ListItem>
          </UnorderedList>

          <Text py="3">
            上記の全てのポイントとそれ以上の内容について、このコースでカバーします。しかし、まず最初に、あなたがContributor Tokenを持っていることを確認しましょう。
          </Text>
          <Text>そのために、このビデオを確認するか、以下の手順に従って、あなたのPPBL2023トークンを作成してください。</Text>
        </GridItem>
        <GridItem colSpan={[2, 1]}>
          <VideoComponent videoId="tD5hVDC06iM">あなたの新しいContributor Tokenについて</VideoComponent>
        </GridItem>
      </Grid>
      <Divider py="5" />

      <AssignmentComponent>
        <Text></Text>
        <OrderedList pb="5" ml="10" w="60%">
          <ListItem py="1" pl="2">あなたのウォレットがCardano Preprod Testnetに接続されていることを確認してください。</ListItem>
          <ListItem py="1" pl="2">担保が設定されていることと、ウォレットでdapp接続が有効になっていることを確認してください。担保とdapp接続について学ぶには、上記のビデオを見てください。</ListItem>
          <ListItem py="1" pl="2">あなたのトークンのエイリアスを選んでください。あなたがブロックチェーン上にトークンを作成していることを忘れずに、あなたのトークンの名前は永久の台帳に記録されます。</ListItem>
          <ListItem py="1" pl="2">Mintボタンをクリックしてください。トランザクションに署名して送信してください。</ListItem>
          <ListItem py="1" pl="2">このページをリフレッシュして、あなたの新しいトークンを確認してください。</ListItem>
          <ListItem py="1" pl="2">
            あなたのカスタムPPBL 2023トークンは直接あなたのウォレットにマイントされます。
          </ListItem>
        </OrderedList>

        <SuccessComponent mastery={connected && connectedPPBL2023Token != undefined}>
          {connected ? (
            <>
              {connectedPPBL2023Token != undefined ? (
                <Box bg="theme.green" color="theme.dark" w="60%" mx="auto" my="5" p="5">
                  <Heading py="3" textAlign="center">
                    &#127881;あなたはPPBL2023トークンを持っています。&#127881; 
                  </Heading>
                  <Text py="3">おめでとうございます。あなたは今、正式にPPBL 2023にサインアップしました!</Text>
                  <Heading size="md" py="3">
                    &#127959; 次へ: &#127959;
                  </Heading>
                  <Box mx="5">
                    <UnorderedList>
                      <ListItem>
                        {" "}
                        このPlutus PBLコースでは、あなたはどのようにPPBL2023トークンがマイントされたかを学び、異なるトークンが接続された場合にアプリケーションが異なる応答をするようにトークンを使用する方法を学びます。
                      </ListItem>
                      <ListItem>
                        モジュール101および102では、新しいContributor Tokenの「ラッキーナンバー」を変更する方法を学びます。
                      </ListItem>
                      <ListItem>
                        また、このWebサイトをハックして、望むだけのPPBL2023トークンをマイントする方法も学びますが、それについては後で説明します。
                      </ListItem>
                    </UnorderedList>
                  </Box>
                  <Text py="3">私たちはあなたと一緒にこの旅を進むことをワクワクしています！</Text>
                </Box>
              ) : (
                <>
                  <ContributorPairMintingComponent />
                  <Divider py="5" />
                  <Heading size="md">あなたが成功したかどうかを知る方法は以下の通りです：</Heading>
                  <Text py="2">あなたのPreprodウォレットにこのポリシーIDを持つトークンが見える場合、あなたは成功したと言えます: </Text>
                  <code>05cf1f9c1e4cdcb6702ed2c978d55beff5e178b206b4ec7935d5e056</code>
                </>
              )}
            </>
          ) : (
            "ウォレットを接続するように確認してください"
          )}
        </SuccessComponent>
      </AssignmentComponent>
    </LessonLayout>
  );
}

