import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Container, Divider, Heading, Box, Text, Button, Link as CLink } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const SLTs100 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <SLTsItems moduleTitle="Module 100" moduleNumber={100} />
      <Divider my="5" />
      <Box bg="theme.lightGray" color="white" p="5">
        <Heading>Plutus PBL 2023 へようこそ！</Heading>
        <Text py="3" fontSize="lg">
          このモジュールの目的は、このコースがどのように機能するかの例を示すことです。 
        </Text>
        <Text py="3" fontSize="lg">
          <Text as="span" fontWeight="900" color="theme.green">
            PBL
          </Text>{" "}
          とは、{" "}
          <Text as="span" fontWeight="900" color="theme.green">
            Project-Based Learningの略称です
          </Text>
          。{" "}
          <Text as="span" fontWeight="900" color="theme.green">
            プロジェクトによって、私たちは実践することで学ぶことができます。
          </Text>
        </Text>
        <Text py="3" fontSize="lg">
          PPBLの各モジュールは、上図のように、Student Learning Targets（SLTs）のリストで始まります。
        </Text>
        <Text py="3" fontSize="lg">
          このモジュールでは、各SLTの「習得」について考えるための異なる方法を見始めることになります。
        </Text>
        <Text py="3" fontSize="lg">
          ページの上部には、これらのSLTに対応するステータスバーがあります。{" "}
          <CLink>
            <Link href="/modules/100/1001">これがどのように機能するかは、レッスン1で見ることができます。</Link>
          </CLink>
        </Text>
        <Text py="3" fontSize="lg">
          次の数回のレッスンでも引き続き案内を行います。
        </Text>
        <Text py="3" fontSize="lg">
          現時点では、サイドバーを使用してレッスン1に移動するか、以下のボタンをタップしてください。
        </Text>
        <Link href="/modules/100/1001">
          <Button my="1em">私はレッスン1の準備ができています!</Button>
        </Link>
      </Box>
    </Box>
  );
};

export default SLTs100;

