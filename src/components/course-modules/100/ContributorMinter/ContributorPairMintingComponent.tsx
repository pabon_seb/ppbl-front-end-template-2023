import {
  Box,
  Text,
  Heading,
  Button,
  FormControl,
  Input,
  FormLabel,
  FormHelperText,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  Grid,
  GridItem,
} from "@chakra-ui/react";
import { Transaction, Mint, Action, AssetMetadata, Data, Recipient, KoiosProvider } from "@meshsdk/core";
import { useWallet, useAddress } from "@meshsdk/react";
import { useFormik } from "formik";
import * as React from "react";
import { useEffect, useState } from "react";
import { contributorPlutusMintingScript } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { contributorReferenceAddress } from "@/src/cardano/plutus/contributorReferenceValidator";
import { hexToString } from "@/src/utils";

const ContributorPairMintingComponent = () => {
  const { connected, wallet } = useWallet();
  const address = useAddress();
  const [asset, setAsset] = useState<Mint | undefined>(undefined);
  const [contribReferenceList, setContribReferenceList] = useState<string[] | undefined>(undefined);

  const koiosProvider = new KoiosProvider("preprod");

  const [contributorTokenName, setContributorTokenName] = useState<string | undefined>(undefined);
  const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null);

  // Chakra Modal -> Successful Minting Tx
  const { isOpen: isSuccessOpen, onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure();
  const { isOpen: isErrorOpen, onOpen: onErrorOpen, onClose: onErrorClose } = useDisclosure();

  const formik = useFormik({
    initialValues: {
      contributorAlias: "",
    },
    onSubmit: (values) => {
      alert("成功です！");
    },
  });

  useEffect(() => {
    const _name = "PPBL2023" + formik.values.contributorAlias;
    setContributorTokenName(_name);
  }, [formik.values.contributorAlias]);

  useEffect(() => {
    const fetchContributorReferenceUTxOs = async () => {
      const tokenNameList: string[] = [];
      const _contributionRefUTxO = await koiosProvider.fetchAddressUTxOs(contributorReferenceAddress);
      console.log(_contributionRefUTxO);
      _contributionRefUTxO.forEach((utxo: any) => {
        const contribUnit = utxo.output.amount[1].unit
        const contribTokenName = hexToString(contribUnit.substring(62))
        tokenNameList.push(contribTokenName)
        // tokenNameList.push(hexToString(utxo.output.amount[1].unit).substring(59));
      });
      console.log(tokenNameList);
      setContribReferenceList(tokenNameList);
    };

    if (contributorReferenceAddress) {
      fetchContributorReferenceUTxOs();
    }
  }, [contributorReferenceAddress]);

  const contributor_datum: Data = {
    alternative: 0,
    fields: [5, []],
  };

  const referenceTokenRecipient: Recipient = {
    address: contributorReferenceAddress,
    datum: {
      value: contributor_datum,
      inline: true,
    },
  };

  // Add a form to enter the Redeemer number
  const redeemer: Partial<Action> = {
    tag: "MINT",
    data: 1618033988,
  };

  const handleMintingTransaction = async () => {
    if (address && contributorTokenName && contributorTokenName != "PPBL2023") {
      const assetMetadata: AssetMetadata = {
        name: contributorTokenName,
        image: "https://www.gimbalabs.com/g.png",
      };
      const _contribAsset: Mint = {
        assetName: "100" + contributorTokenName,
        assetQuantity: "1",
        metadata: assetMetadata,
        label: "721",
        recipient: referenceTokenRecipient,
      };
      const _referenceAsset: Mint = {
        assetName: "222" + contributorTokenName,
        assetQuantity: "1",
        metadata: assetMetadata,
        label: "721",
        recipient: address,
      };
      try {
        const tx = new Transaction({ initiator: wallet });
        tx.mintAsset(contributorPlutusMintingScript, _contribAsset, redeemer);
        tx.mintAsset(contributorPlutusMintingScript, _referenceAsset, redeemer);
        const unsignedTx = await tx.build();
        const userSignedTx = await wallet.signTx(unsignedTx, true);
        const txHash = await wallet.submitTx(userSignedTx);
        console.log("成功!", txHash);
        setSuccessfulTxHash(txHash);
        onSuccessOpen();
      } catch (error: any) {
        alert(error);
        console.log(error);
      }
    } else {
      alert("トークンの一意のエイリアスを入力します。");
    }
  };

  return (
    <>
      <Box borderColor="theme.four" fontSize="lg" lineHeight="9">
        <Heading size="md" py="3">
          Contributor Tokenペアをミントする
        </Heading>
        <FormControl color="theme.dark">
          <FormLabel color="theme.light">ここにあなたのエイリアスを書いてください。</FormLabel>
          <Input
            mb="3"
            bg="white"
            id="contributorAlias"
            name="contributorAlias"
            onChange={formik.handleChange}
            value={formik.values.contributorAlias}
            placeholder="トークンにエイリアスを入力してください。"
          />
          <FormHelperText py="2">このボタンにあなたのトークンの名前をプレビューしてください：</FormHelperText>
        </FormControl>
        {contributorTokenName && contribReferenceList && contribReferenceList.includes(contributorTokenName) ? (
          <Text>ユニークなトークン名を選択してください。</Text>
        ) : (
          <Button colorScheme="green" onClick={handleMintingTransaction}>
            あなたの{contributorTokenName}を発行してください。
          </Button>
        )}
        {contribReferenceList && (
          <Accordion allowToggle py="5">
            <AccordionItem>
              <AccordionButton>
                <Text py="3">
                  あなたのトークン名はユニークですか？ ここをクリックして、これまでに発行されたPPBL2023トークンのリストを表示してください。
                </Text>
              </AccordionButton>
              <AccordionPanel>
                <Grid templateColumns="repeat(5, 1fr)" gap={3}>
                  {contribReferenceList.map((tName: any, i: number) => (
                    <GridItem key={i} p="1" bg="theme.light" color="theme.dark">
                      <Text fontSize="sm" fontWeight="bold">
                        {tName}
                      </Text>
                    </GridItem>
                  ))}
                </Grid>
              </AccordionPanel>
            </AccordionItem>
          </Accordion>
        )}
      </Box>
      <Modal
        key="successMintingModal"
        size="lg"
        blockScrollOnMount={false}
        isOpen={isSuccessOpen}
        onClose={onSuccessClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="3xl">成功：トークン発行トランザクションが送信されました。</ModalHeader>
          <ModalBody>
            <Text py="2">Minting Transaction Hash: {successfulTxHash}</Text>
            <Text py="2">
              このトランザクションがブロックチェーンエクスプローラーに表示されるには数分かかる場合があります。数分待ってから、このページを更新して変更を確認してください。
            </Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="white" color="gray.700" onClick={onSuccessClose}>
              閉じる
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default ContributorPairMintingComponent;
