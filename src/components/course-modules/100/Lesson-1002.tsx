import AssignmentComponent from "@/src/components/lms/Lesson/AssignmentComponent";
import SuccessComponent from "@/src/components/lms/Lesson/SuccessComponent";
import VideoComponent from "@/src/components/lms/Lesson/VideoComponent";
import SLT from "@/src/components/ui/Text/SLT";
import {
  Box,
  Grid,
  ListItem,
  OrderedList,
  Stack,
  StackDivider,
  Text,
  Link as CLink,
  Button,
  GridItem,
  Divider,
} from "@chakra-ui/react";
import { CardanoWallet, useAddress, useLovelace, useNetwork, useWallet } from "@meshsdk/react";
import { useEffect, useState } from "react";
import Link from "next/link";
import LessonLayout from "../../lms/Lesson/LessonLayout";

export default function Lesson1002() {
  const { connected, wallet } = useWallet();
  const address = useAddress();
  const network = useNetwork();
  const lovelace = useLovelace();

  const [mastery, setMastery] = useState(false);

  useEffect(() => {
    if (network == 0 && lovelace && parseInt(lovelace) > 0) {
      setMastery(true);
    }
  }, [connected, network, lovelace]);

  return (
    <LessonLayout moduleNumber={100} sltId="100.2" slug="1002">
      <Grid mx="auto" fontWeight="bold" lineHeight="200%" templateColumns="repeat(2, 1fr)" gap={6}>
        <GridItem w="90%" mx="auto" colSpan={[2, 1]}>
          <Text fontSize="lg" py="3">
            Cardanoでアプリケーションを開発し、テストすることを開発者にとって簡単にするため、IOHKは{" "}
            <CLink href="https://docs.cardano.org/" target="_blank">Cardanoエコシステムドキュメンテーション</CLink>内に{" "}
            <CLink href="https://docs.cardano.org/cardano-testnet/tools/faucet" target="_blank">Testnet Faucet</CLink>と呼ばれるものを提供しています。
          </Text>
          <Text fontSize="lg" py="3">
            「test-Ada」または「tAda」をPreprodテストネット上で入手する方法を知りたい場合は、このビデオを見るか、以下の手順に従ってください。
          </Text>
        </GridItem>
        <GridItem colSpan={[2, 1]}>
          <VideoComponent videoId="CnXzI4B_pmU">Preprodの蛇口からtAdaを入手する方法</VideoComponent>
        </GridItem>
      </Grid>
      <Divider py="5" />

      <AssignmentComponent>
        <OrderedList pb="5">
          <ListItem>
            {" "}
            <CLink href="https://docs.cardano.org/cardano-testnet/tools/faucet" target="_blank">
              https://docs.cardano.org/cardano-testnet/tools/faucet
            </CLink>にアクセスしてください。
          </ListItem>
          <ListItem>「Environment」メニューから「Preprod Testnet」を必ず選択してください。</ListItem>
          <ListItem>Preprodのアドレスを入力し、フォームを送信してください。</ListItem>
          <ListItem>
            「tAda」が届くまでしばらくお待ちください。その後、このページを更新してウォレットを再接続してください。
          </ListItem>
        </OrderedList>
        <SuccessComponent mastery={connected && mastery}>
          {mastery ? (
            <Box>
              <Text>おめでとうございます! 今、あなたはtAdaでお金持ちになりました！</Text>
            </Box>
          ) : (
            <Box>
              <Text>あなたのPreprodウォレットにtAdaがあれば、成功していることがわかります。</Text>
            </Box>
          )}
        </SuccessComponent>
      </AssignmentComponent>
    </LessonLayout>
  );
}
