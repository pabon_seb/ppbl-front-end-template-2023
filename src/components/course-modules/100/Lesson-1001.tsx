import AssignmentComponent from "@/src/components/lms/Lesson/AssignmentComponent";
import SuccessComponent from "@/src/components/lms/Lesson/SuccessComponent";
import VideoComponent from "@/src/components/lms/Lesson/VideoComponent";
import SLT from "@/src/components/ui/Text/SLT";
import {
  Box,
  Button,
  Container,
  Divider,
  Grid,
  GridItem,
  Heading,
  ListItem,
  OrderedList,
  Stack,
  StackDivider,
  Text,
  Link as CLink,
} from "@chakra-ui/react";
import { CardanoWallet, useAddress, useNetwork, useWallet } from "@meshsdk/react";
import Link from "next/link";
import LessonLayout from "../../lms/Lesson/LessonLayout";

export default function Lesson1001() {
  const { connected, wallet } = useWallet();
  const address = useAddress();
  const network = useNetwork();

  return (
    <LessonLayout moduleNumber={100} sltId="100.1" slug="1001">
      <Grid mx="auto" fontSize="xl" fontWeight="semibold" lineHeight="200%" templateColumns="repeat(2, 1fr)" gap={6}>
        <GridItem w="90%" mx="auto" colSpan={[2, 1]}>
          <Text py="3">
            普通、私たちがCardanoと話している場合、実際には「Cardano Mainnet」について話していることが多いです。
          </Text>
          <Text py="3">
            しかし、MainnetだけがCardanoのネットワークではありません。
          </Text>

          <Text py="3">
            私たちはCardano Mainnet上で新しい分散型アプリケーションをテストしたくありません。: もしミスをした場合、実際に価値のあるトークンを失う可能性があります!
          </Text>
          <Text py="3">
            そこで本講座では、Cardanoの「プレプロダクションテストネットワーク」、通称「Preprod」を使用します。
          </Text>
          <Text py="3">
            あなたの最初のPPBL2023ミッション：ブラウザベースのウォレットをPreprodに接続します。
          </Text>
        </GridItem>
        <GridItem colSpan={[2, 1]}>
          <VideoComponent videoId="pjdXKsRXtzw">あなたのウォレットをPre-Production Testnetに接続する方法:</VideoComponent>
        </GridItem>
      </Grid>
      <Divider py="5" />

      <AssignmentComponent>
        <OrderedList>
          <ListItem><CLink href="https://namiwallet.io/" target="_blank">Nami</CLink>や<CLink href="https://eternl.io/" target="_blank">Eternl</CLink>のようなブラウザウォレットがインストールされていることを確認してください。</ListItem>
          <ListItem>Preprodにウォレットを接続する方法を確認するために、上の短い動画を見てください。</ListItem>
          <ListItem>成功したかどうかを確認するために、下の「Connect Wallet」ボタンを使用してください!</ListItem>
        </OrderedList>
        <Text py="5">ウォレットを接続しようとしてください。Cardano Pre-Productionのテストネットワーク上にあることを確認してください。</Text>
        <Box mb="20">
          <CardanoWallet />
        </Box>
        <SuccessComponent mastery={connected && network == 0}>
          <>
            {network == 0 ? (
              <>
                <Text color="theme.green" fontWeight="bold">
                  現在、Cardanoのテストネットに接続されました。ご住所です：
                </Text>
                <Text fontSize="sm" py="2">
                  {address}
                </Text>
                <Text py="5">
                  あなたは素晴らしい仕事をしています。 次のレッスンでは、あなたのPreprod財布にいくつかの「test Ada」を取得します。
                </Text>
              </>
            ) : (
              <Text>あなたのウォレットはプレプロダクションのテストネットワークに接続されています。</Text>
            )}
          </>
        </SuccessComponent>
      </AssignmentComponent>
    </LessonLayout>
  );
}
