import AssignmentComponent from "@/src/components/lms/Lesson/AssignmentComponent";
import SuccessComponent from "@/src/components/lms/Lesson/SuccessComponent";
import SLT from "@/src/components/ui/Text/SLT";
import { Button, Grid, Stack, StackDivider, Text, Heading, GridItem, Divider, Link as CLink } from "@chakra-ui/react";
import Link from "next/link";
import LessonLayout from "../../lms/Lesson/LessonLayout";
import VideoComponent from "../../lms/Lesson/VideoComponent";

export default function Lesson1003() {
  return (
    <LessonLayout moduleNumber={100} sltId="100.3" slug="1003">
      <Grid mx="auto" fontWeight="bold" lineHeight="200%" templateColumns="repeat(2, 1fr)" gap={6}>
        <GridItem w="90%" mx="auto" colSpan={[2, 1]}>
          <Text fontSize="lg" py="3">
            私たちは、このPPBLコースが、あなたが最初の2つのSLTを習得したことをあなたに伝えることができるのは素晴らしいことだと思っています。しかし、習得度合いは常に直接的に測定できるわけではありません。 
          </Text>
          <Text fontSize="lg" py="3">
            ブロックチェーンは、私たち全員が自分の「プライベートキー」と「ニーモニック」フレーズを注意深く安全に保管する必要があります。あなたがこの学習目標を習得したかどうかの最も重要な判断者はあなた自身です。 
          </Text>
          <Text fontSize="lg" py="3">
            2023年3月16日(木)UTC 1430に開催される「Plutus PBLライブコーディング」で、ウォレットセキュリティについて話し合いましょう。このセッションは録画され、<CLink><Link href="/live-coding">Plutus PBLライブコーディングセッション</Link></CLink>ページに投稿されますので、参加してください。
          </Text>
        </GridItem>
        <GridItem colSpan={[2, 1]}>
          <VideoComponent videoId="bFrBf7IJmWo">Private Keys</VideoComponent>
        </GridItem>
      </Grid>
      <Divider py="5" />

      <AssignmentComponent>
        <Text py="2">Cardanoで開発を行う場合、あなたの鍵を扱う方法はいくつかあります。</Text>

        <Text py="2">
          今のところは、あなたの「mnemonic」単語が「offline」で書かれ、複数の安全なコピーがあることを確認してください。
        </Text>
        <Text py="2" width="70%">
          モジュール101では、cardano-cli上の鍵の管理について話し合います。モジュール201では、ウェブアプリケーションに鍵を保存する方法を調査し、Lesson 100.4をハックする方法を学びます。
        </Text>
        <SuccessComponent mastery={false}>あなたは、他の誰もあなたの鍵にアクセスできないと自信を持っています。</SuccessComponent>
      </AssignmentComponent>
    </LessonLayout>
  );
}
