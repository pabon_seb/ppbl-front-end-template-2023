import { Container, Divider, Box, Button, Spacer, Flex, Heading, Grid, GridItem, Text } from "@chakra-ui/react";
import Link from "next/link";
import React, { useContext, useState } from "react";

import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import Commit from "@/src/components/course-modules/103/Commit.mdx";
import { PPBLContext } from "@/src/context/PPBLContext";
import { hexToString } from "@/src/utils";

const Commit103 = () => {
  const [selectedProject, setSelectedProject] = useState("");

  const ppblContext = useContext(PPBLContext);

  const handleChooseProject = (project: string) => {
    setSelectedProject(project);
  };

  return (
    <CommitLayout moduleNumber={103} slug="commit">
      <Grid templateColumns="repeat(5, 1fr)" templateRows="repeat(2, 1fr)" gap={5}>
        <GridItem colSpan={[5, 5, 5, 5, 5, 3]} rowSpan={2}>
          <Commit />
        </GridItem>
        <GridItem colSpan={[5, 5, 5, 5, 5, 2]} border="1px" borderColor="theme.yellow" borderRadius="md" p="3">
          <Heading size="lg" fontWeight="200">
            モジュール103にCommitする。
          </Heading>
          {ppblContext.connectedContribToken && (
            <Text pb="3" fontWeight="bold" color="theme.yellow">
              接続された「PPBL 2023 Token（PPBL 2023 トークン）」: {ppblContext.connectedContribToken}
            </Text>
          )}
          <Box mb="3" p="3" bg="theme.yellow" color="theme.dark">
            <Text>
              モジュール103には2つの方法で「コミット」できます。レッスン103.1の課題を完了するか、スキップすることができます。レッスン103.1を完了しなかった場合でも、「Module103 no GitLab」を選択することで、GPTEの動作を確認できます。  
            </Text>
          </Box>
          <Grid templateColumns={["repeat(1, 1fr)", "repeat(1, 1fr)", "repeat(1, 1fr)", "repeat(2, 1fr)"]} gap="5">
            <Button key={null} colorScheme="green" onClick={() => handleChooseProject("Module103 with GitLab")}>
              Module103 with GitLab
            </Button>
            <Button key={null} colorScheme="green" onClick={() => handleChooseProject("Module103 no GitLab")}>
              Module103 no GitLab
            </Button>
          </Grid>
          <Box my="5">
            {selectedProject && ppblContext.treasuryUTxO && <CommitmentTx selectedProject={selectedProject} />}
          </Box>
        </GridItem>
      </Grid>
    </CommitLayout>
  );
};

export default Commit103;
