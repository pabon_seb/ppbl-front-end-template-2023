import Assignment2021 from "@/src/components/course-modules/202/Assignment2021.mdx";
import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import { Grid, GridItem, Text, Box, Heading, useColorModeValue } from "@chakra-ui/react";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import { useContext, useEffect, useState } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import Link from "next/link";
import { useWallet } from "@meshsdk/react";
import { checkReferenceDatumForPrerequisite, stringToHex } from "@/src/utils";

const Assignment2021Page = () => {
  const ppblContext = useContext(PPBLContext);
  const { connected } = useWallet();
  const [hasPrerequisite, setHasPrerequisite] = useState(false);
  const text_color = useColorModeValue("theme.orange", "theme.yellow");

  useEffect(() => {
    if (ppblContext.contributorReferenceDatum) {
      setHasPrerequisite(
        checkReferenceDatumForPrerequisite(ppblContext.contributorReferenceDatum, [stringToHex("Module201")])
      );
    }
  }, [ppblContext.contributorReferenceDatum]);

  return (
    <CommitLayout moduleNumber={202} slug="assignment2021">
      <Grid templateColumns="repeat(5, 1fr)" templateRows="repeat(2, 1fr)" gap={5}>
        <GridItem colSpan={[5, 5, 5, 5, 5, 3]} rowSpan={2}>
          <Assignment2021 />
        </GridItem>
        <GridItem colSpan={[5, 5, 5, 5, 5, 2]} border="1px" borderColor={text_color} borderRadius="md" p="3">
          {hasPrerequisite ? (
            <>
              <Box my="5">{ppblContext.treasuryUTxO && <CommitmentTx selectedProject={"Module202"} />}</Box>
            </>
          ) : (
            <>
              <Box my="5">
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  準備ができたら、モジュール202へのcommitmentを行ってください。ルールをよく読むことを忘れないでください。
                </Text>
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  モジュール202にコミットする前に、 <Link href="/modules/201/commit">モジュール201のコミットメント</Link> を完了する必要があります。
                </Text>
                {ppblContext.connectedContribToken && (
                  <Text fontSize="sm" fontWeight="300">
                    {ppblContext.connectedContribToken}
                  </Text>
                )}
                {!connected && (
                  <Text fontSize="sm" fontWeight="300">
                    完了ステータスを確認するには、ウォレットを接続してください。
                  </Text>
                )}
              </Box>
            </>
          )}
        </GridItem>
      </Grid>
    </CommitLayout>
  );
};

export default Assignment2021Page;
