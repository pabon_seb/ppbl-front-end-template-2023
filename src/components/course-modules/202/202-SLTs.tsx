import SLTsItems from "@/src/components/lms/Lesson/SLTs";
import { Divider, Heading, Box } from "@chakra-ui/react";
import React from "react";
import Introduction from "@/src/components/course-modules/202/Introduction.mdx";
import LessonNavigation from "@/src/components/lms/Lesson/LessonNavigation";

const SLTs202 = () => {
  return (
    <Box w="95%" marginTop="2em">
      <SLTsItems moduleTitle="Module 202" moduleNumber={202} />
      <Divider mt="5" />
      <Box py="5">
        <Introduction />
      </Box>
      <LessonNavigation moduleNumber={202} currentSlug="slts" />
    </Box>
  );
};

export default SLTs202;