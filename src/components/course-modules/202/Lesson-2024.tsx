import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module202.json";
import Docs2024 from "@/src/components/course-modules/202/Docs2024.mdx";

export default function Lesson2024() {
  const slug = "2024";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={202} sltId="202.4" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2024 />
    </LessonLayout>
  );
}
