import { Flex, Box, Heading, Text } from "@chakra-ui/react";
import * as React from "react";
import { StatusBox } from "@/src/components/lms/Status/StatusBox";
import { useAddress, useAssets } from "@meshsdk/react";
import { NativeScript, resolveNativeScriptHash, resolvePaymentKeyHash } from "@meshsdk/core";
import { TX_FROM_ADDRESS_WITH_POLICYID } from "../102/queries";
import { useLazyQuery } from "@apollo/client";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { PPBLContext } from "@/src/context/PPBLContext";

type Props = {
  children?: React.ReactNode;
};

const StudentPlaceholderComponent: React.FC<Props> = ({ children }) => {
  const walletAssets = useAssets();
  const address = useAddress(0);

  const [cliTokenPresent, setCliTokenPresent] = React.useState(false);
  const [cliTokenPolicyId, setCliTokenPolicyId] = React.useState<string | undefined>(undefined);

  const [meshTokenPresent, setMeshTokenPresent] = React.useState(false);
  const [meshTokenPolicyId, setMeshTokenPolicyId] = React.useState<string | undefined>(undefined);

  const [plutusTxTokenPresent, setPlutusTxTokenPresent] = React.useState(false);
  const plutusTxTokenPolicyId = "b4dff8a4bf58ef312cfc498231d4385349cdf9bc39e3bd0278f7637e";

  const [aikenTokenPresent, setAikenTokenPresent] = React.useState(false);
  const aikenTokenPolicyId = "9ff7cbc943842fff04e6b3b5a4abf9068bb73bccd2f314f5c34a9343";

  const ppblContext = React.useContext(PPBLContext);

  React.useEffect(() => {
    // Module 202.2: Native Script CLI
    if (ppblContext.cliAddress && ppblContext.cliAddress !== "" && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(ppblContext.cliAddress);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setCliTokenPolicyId(policyId);
      setCliTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
    }

    // Module 202.3: Mesh
    if (address && walletAssets !== undefined) {
      const pubKeyHash = resolvePaymentKeyHash(address);
      const nativeScript: NativeScript = {
        type: "sig",
        keyHash: pubKeyHash,
      };
      const policyId = resolveNativeScriptHash(nativeScript);
      setMeshTokenPolicyId(policyId);
      setMeshTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == policyId));
      setPlutusTxTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == plutusTxTokenPolicyId));
      setAikenTokenPresent(walletAssets?.some((a) => a.unit.substring(0, 56) == aikenTokenPolicyId));
    }
  }, [walletAssets, address, ppblContext.cliAddress]);

  return (
    <>
      <Box border="1px" p="3" my="5">
        <Text>予想されるポリシーID:</Text>
        <Text>
          {" "}
          {ppblContext.connectedContribToken ? (
            <Text>PPBL 2023 トークン: {ppblContext.connectedContribToken}</Text>
          ) : (
            <Text>PPBL 2023 トークンが見つかりませんでした。</Text>
          )}
        </Text>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.2</Text>
          <Text>
            このブラウザウォレットに関連付けられたCLIアドレスは何ですか？{" "}
            {ppblContext.cliAddress ? ppblContext.cliAddress : "CLIアドレスは見つかりませんでした。"}
          </Text>

          {cliTokenPolicyId ? (
            <Text>
              もし「CLIウォレット」の「Public Key Hash」を使用してトークンを「鋳造 (mint)」した場合、それは以下の「Policy ID」を持つことになります:{" "}
              {cliTokenPolicyId}
            </Text>
          ) : (
            <Text>予想されるポリシーIDを表示するには、ウォレットを接続してください。</Text>
          )}
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.3</Text>
          {meshTokenPolicyId ? (
            <Text>
              接続されたブラウザウォレットの「Public Key Hash」を使用してトークンを「鋳造 (mint)」した場合、それは以下の「Policy ID」を持つことになります:{" "}
              {meshTokenPolicyId}
            </Text>
          ) : (
            <Text>予想されるポリシーIDを表示するには、ウォレットを接続してください。</Text>
          )}
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.4</Text>
          <Text>予想される PlutusTx ポリシー ID: {plutusTxTokenPolicyId}</Text>
        </Box>
        <Box bg="theme.light" color="theme.dark" px="3" m="1">
          <Text>SLT 202.5</Text>
          <Text>予想される「Aiken」のポリシーID： {aikenTokenPolicyId}</Text>
          <Text fontSize="sm" fontWeight="bold">注意: Cardanoスマートコントラクト言語が更新されると、異なるPolicyIDが生成されることがあります。ここに示されているものと同じPolicyIDが表示されない場合は、インストールされている「Aiken」のバージョンを確認し、質問がある場合はDiscordに連絡してください。</Text>
          <Text fontSize="sm" fontWeight="bold">2023-09-04更新 Aikenバージョン <pre>1.0.16-alpha</pre></Text>
        </Box>
      </Box>
    </>
  );
};

export default StudentPlaceholderComponent;
