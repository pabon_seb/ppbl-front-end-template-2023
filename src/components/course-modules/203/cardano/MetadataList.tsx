import { useQuery, gql } from "@apollo/client";
import { Box, Center, Flex, Grid, Heading, Spacer, Spinner, Stack, Text } from "@chakra-ui/react";
import { useWallet } from "@meshsdk/react";

type Props = {
  metadataKey: string;
};

const QUERY = gql`
  query TransactionsWithMetadataKey($metadatakey: String!) {
    transactions(where: { metadata: { key: { _eq: $metadatakey } } }, order_by: { includedAt: desc }) {
      hash
      includedAt
      metadata {
        key
        value
      }
    }
  }
`;

const MetadataList: React.FC<Props> = ({ metadataKey }) => {
  const { data, loading, error } = useQuery(QUERY, {
    variables: {
      metadatakey: metadataKey,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }
  
  return (
    <Box p="2" my="3" border="1px" borderColor="theme.light">
      <Heading textAlign="center">Hello PPBL 2023!</Heading>
      <Text textAlign="center" mb="5">あなたの &quot;Hello PPBL 2023&quot; メッセージをこのリストに追加するための、上記の課題を完了してください。</Text>
      <Grid templateColumns="repeat(2, 1fr)" gap={5}>
        {data.transactions.map((tx: any | null | undefined, index: number) => (
          <Box my="2" bg="theme.light" color="theme.dark" key={index}>
            <Box p="3" bg="theme.dark" color="theme.light">
              <Heading size="sm" py="2">
                Tx Metadata:
              </Heading>
              {tx.metadata.map((metadata: any, index: number) => (
                <Text py="1" key={index}>
                  key: {JSON.stringify(metadata.key)} value: {JSON.stringify(metadata.value)}
                </Text>
              ))}
            </Box>
            <Text>
              Tx Hash: <a href={`https://testnet.cardanoscan.io/transaction/${tx.hash}`}>{tx.hash}</a>
            </Text>
            <Text>Tx Date: {tx.includedAt}</Text>
          </Box>
        ))}
      </Grid>
    </Box>
  );
};

export default MetadataList;
