import Assignment2031 from "@/src/components/course-modules/203/Assignment2031.mdx";
import CommitLayout from "@/src/components/lms/Lesson/CommitLayout";
import { Grid, GridItem, Text, Box, Heading, useColorMode, useColorModeValue } from "@chakra-ui/react";
import CommitmentTx from "@/src/components/gpte/transactions/CommitmentTx";
import { useContext, useEffect, useState } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import Link from "next/link";
import { useAssets, useWallet } from "@meshsdk/react";
import { getInlineDatumForContributorReference } from "@/src/data/queries/getInlineDatumForContributorReference";
import { checkReferenceDatumForPrerequisite } from "@/src/utils";
import NFTGallery from "./cardano/NFTGallery";

const Assignment2031Page = () => {
  const ppblContext = useContext(PPBLContext);
  const { connected } = useWallet();
  const walletAssets = useAssets();
  const [hasPrerequisite, setHasPrerequisite] = useState(false);
  const [refDatum, setRefDatum] = useState<any | undefined>(undefined);
  const text_color = useColorModeValue("theme.orange", "theme.yellow");

  useEffect(() => {
    const fetchDatum = async () => {
      if (ppblContext.connectedContribToken && ppblContext.connectedContribToken.length > 0) {
        const _datum = await getInlineDatumForContributorReference(ppblContext.connectedContribToken);
        setRefDatum(_datum);
      }
    };

    if (ppblContext.connectedContribToken) {
      fetchDatum();
    }
  }, [ppblContext.connectedContribToken]);

  useEffect(() => {
    if (walletAssets) {
      setHasPrerequisite(
        walletAssets.some((a) => a.unit.substring(0, 56) == "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86")
      );
    }
  }, [walletAssets]);

  return (
    <>
      <CommitLayout moduleNumber={203} slug="assignment2031">
        <Grid templateColumns="repeat(5, 1fr)" templateRows="repeat(2, 1fr)" gap={5}>
          <GridItem colSpan={{ base: 5, lg: 3 }} rowSpan={2}>
            <Assignment2031 />
          </GridItem>
          <GridItem colSpan={{ base: 5, lg: 2 }} border="1px" borderColor={text_color} borderRadius="md" p="3">
            <>
              <Box my="5">
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  あなたの「PPBL 2023 Contributor Token」は、この課題で使用される「minting」バリデータによって必要とされます。
                  あなたの「PPBL 2023 Contributor Token」はまた、この課題にcommitするためにも必要です。
                  最初に課題を完了してください。成功した場合、ウォレットに「PPBL 2023 NFT」があることを知るでしょう。              
                </Text>
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  その後、モジュール203にコミットできます。
                </Text>
                {ppblContext.connectedContribToken && (
                  <Text fontSize="sm" fontWeight="300">
                    接続された『PPBL 2023 Contributor Token』: {ppblContext.connectedContribToken}
                  </Text>
                )}
                {!connected && (
                  <Text fontSize="sm" fontWeight="300">
                    ウォレットを接続して完了状況を確認してください。
                  </Text>
                )}
              </Box>
            </>
            {hasPrerequisite && (
              <>
                <Text fontSize="lg" fontWeight="900" color={text_color} pb="3">
                  成功しました！「commitment」を行い、モジュール203でのマスタリーステータスを取得してください：
                </Text>
                <Box my="5">{ppblContext.treasuryUTxO && <CommitmentTx selectedProject={"Module203"} />}</Box>
              </>
            )}
          </GridItem>
        </Grid>
      </CommitLayout>
    </>
  );
};

export default Assignment2031Page;
