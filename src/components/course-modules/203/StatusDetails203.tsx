import { Flex, Box, Heading, Text, Link as CLink, Divider } from "@chakra-ui/react";
import * as React from "react";
import { StatusBox } from "@/src/components/lms/Status/StatusBox";
import { useAddress, useAssets } from "@meshsdk/react";
import { NativeScript, resolveNativeScriptHash, resolvePaymentKeyHash } from "@meshsdk/core";
import { useLazyQuery } from "@apollo/client";
import { contributorTokenPolicyId } from "@/src/cardano/plutus/contributorPlutusMintingScript";
import { PPBLContext } from "@/src/context/PPBLContext";
import { GraphQLTransactionMetadata } from "@/src/types/cardanoGraphQL";
import { ADDRESS_METADATA_QUERY } from "@/src/data/queries/metadataQueries";
import Link from "next/link";

const ppblNFTPolicyId = "2a384dc205a97463577fc98b704b537f680c0eba84126eb7d5857c86";

type Props = {
  children?: React.ReactNode;
};

const StatusDetails203: React.FC<Props> = ({ children }) => {
  const walletAssets = useAssets();
  const address = useAddress(0);

  const ppblContext = React.useContext(PPBLContext);

  const [lesson2031metadata, setLesson203metadata] = React.useState<GraphQLTransactionMetadata[] | undefined>(
    undefined
  );
  const [ppblNFT, setPPBLNFT] = React.useState(false);

  const [metadataQuery, { data: metadataQueryData }] = useLazyQuery(ADDRESS_METADATA_QUERY);

  React.useEffect(() => {
    // Module 203.1: Metadata at key 2023
    // Does not check for Mesh/GameChanger completion of metadata tx
    if (ppblContext.cliAddress) {
      metadataQuery({
        variables: {
          walletAddress: ppblContext.cliAddress,
          metadataKey: "2023",
        },
      });
    }

    // Module 203.2: PPBL NFT
    if (walletAssets) {
      setPPBLNFT(walletAssets.some((a) => a.unit.substring(0, 56) == ppblNFTPolicyId));
    }
  }, [ppblContext.cliAddress, metadataQuery, walletAssets]);

  React.useEffect(() => {
    if (metadataQueryData && metadataQueryData.transactions.length > 0) {
      const _metadata: GraphQLTransactionMetadata[] = [];
      metadataQueryData.transactions.forEach((tx: any) => {
        _metadata.push(tx.metadata[0]);
      });
      setLesson203metadata(_metadata);
    }
  }, [metadataQueryData]);

  return (
    <>
      <Box p="3" bg="theme.dark" border="1px" borderColor="theme.light" borderRadius="md">
        <Text fontSize="xl" fontWeight="bold">
          モジュール203のマスタリー詳細
        </Text>
        <h2>On-chain証拠: SLT 203.1 および 203.2 </h2>
        <Text>ステータス詳細を表示するには、ウォレットを接続してください。</Text>
        <Text>あなたのCLIアドレス: {ppblContext.cliAddress}</Text>
        {lesson2031metadata && (
          <>
            <Text fontSize="2xl">レッスン 203.1: Metadata</Text>
            <pre>{JSON.stringify(lesson2031metadata, null, 2)}</pre>
          </>
        )}

        {ppblNFT && (
          <>
            <Text fontSize="2xl">レッスン 203.2: Mint a PPBL NFT</Text>

            <Text>
              あなたは Policy IDとしてPPBL 2023トークンを『発行 (minted)』しました {ppblNFTPolicyId}。あなたのNFTのmintingトランザクション
              のmetadataが有効かどうかは、<CLink href="/modules/203/nft-gallery">NFT Gallery</CLink>を確認することで確認できます。.
              あなたは、自分のNFTと追加した画像を見ることができますか?
            </Text>
          </>
        )}
<Divider />
        <h2>ブロックチェーン上にまだ証拠はありません(今のところ): SLT 203.3 および 203.4</h2>
        
        <Text fontSize="2xl">Lesson 203.3: Cardano Improvement Proposalsを読む</Text>
        <Text>
          どの{" "}
          <CLink href="https://cips.cardano.org/" target="_blank">
            Cardano Improvement Proposals
          </CLink>{" "}
          があなたにとって最も興味深いですか?
        </Text>
        <Text fontSize="2xl">Lesson 203.4: 新しいStandardsへの貢献</Text>
        <Text>
          <Link href="/modules/100/1004">Lesson 100.4</Link>で「鋳造(minted)」
          したPPBL 2023 Contributor tokenを使用して、{" "}
          <CLink href="https://cips.cardano.org/cips/cip68/" target="_blank">
            Cardano Improvement Proposal 68
          </CLink>
          で説明されているアイデアの一部を試験しています。
        </Text>
      </Box>
    </>
  );
};

export default StatusDetails203;
