# レッスン3: 「Minter」バリデータ

今、この言語の知識をいくらか身につけたので、スマートコントラクトの作成にさらに深く入ってみましょう。このレッスンでは、[203.1の課題](/modules/203/assignment2031)から「minter」バリデータを改めて取り上げますが、今回はAikenで書き直します。[実装](https://gitlab.com/gimbalabs/ppbl-2023/ppbl-aiken-starter/-/blob/main/validators/minter.ak?ref_type=heads)は、**ppbl-aiken-starter**リポジトリ内の validators フォルダーにあります。興味があれば、PlutusTxのソースコードも[こちら](https://gitlab.com/gimbalabs/ppbl-2023/ppbl2023-plutus-template/-/blob/main/src/PPBLNFT/Minter.hs)で確認できます。ステップバイステップで説明しますが、まずはいくつかの重要な概念を簡単に振り返りましょう。

## Redeemerと Script Context。

minting バリデータは、"on-chain" 実行のために2つのパラメーター、Redeemer と script context を受け取ります。

1. **Redeemer** は、トランザクションを開始するユーザーによって提供されるデータの一部です。これは、バリデータスクリプトで指定された 「spending」 条件を証明するために使用されます。たとえば、スマートコントラクトが資金の支出に特定の条件を満たす必要がある場合、リディーマーにはその条件を満たすために必要な証明やデータが含まれます。

例えば、Redeemerは特定のトークンが1つだけ「minted（鋳造される）」ことを制御できます。また、トークンに割り当てられる `Token Name` が以前に定義された `Token Name` と対応していることを確認できます。

2. **Script context** は、現在のトランザクションに関する関連情報を提供するインタフェースです。これには、入力、出力、およびトランザクションに関連するその他のコンテキスト情報などの詳細が含まれます。 Script context は、スマートコントラクトが所属するトランザクションに関する情報にアクセスして処理することを可能にし、そのコンテキストに基づいて情報を収集し、特定のルールを強制することができます。以下は、Aiken での表現です。

```haskell
ScriptContext { transaction: Transaction, purpose: ScriptPurpose }
```

もしも`transaction`コンストラクターを見ると、以下のような内容が確認できます。

```haskell
Transaction {
  inputs: List<Input>, 
  reference_inputs: List<Input>,
  outputs: List<Output>,
  fee: Value,
  mint: MintedValue,
  certificates: List<Certificate>,
  withdrawals: Dict<StakeCredential, Int>,
  validity_range: ValidityRange,
  extra_signatories: List<Hash<Blake2b_224, VerificationKey>>,
  redeemers: Dict<ScriptPurpose, Redeemer>,
  datums: Dict<Hash<Blake2b_256, Data>, Data>,
  id: TransactionId,
}
```

こちらにはたくさんの有用な情報があります。この特定の「minter」バリデーターに関して、トランザクションの`inputs`、鋳造された`value`、そして`purpose`コンストラクターに含まれる`policy ID`を検査します。さらに、script contextに関する詳細については、標準ライブラリAPIドキュメントの[transaction section](https://aiken-lang.github.io/stdlib/aiken/transaction.html)を探索することをお勧めします。

## それはパラメータ化されたバリデーターです。

既に、`datum`、`redeemer`、および`script context`に加えて、コントラクトをカスタマイズするために追加のパラメーターを指定することができることを知っています。これは、私たちのminterバリデーターの場合に該当します。以下のデータを適用することができます。

```rust
type PolicyParams {
  contributor: PolicyId,
}
```

契約を直接Aikenからパラメータ化することができます。これを行うには、以下のコマンドを実行する必要があります。

```bash
$ aiken blueprint apply minter.minter ...
```

おっと！エラーメッセージですね。このレッスンの最後に、最初に再実装しなければならないいくつかの関数を書く演習があります。

Aikenでは、これらの追加のパラメーターは`validator`キーワードの隣に配置されるべきだということを言及する価値があります。他のパラメーター（`datum`、`redeemer`、`script context`）は、`validator`の中括弧の内側の関数に入れる必要があります。以下のようになります。

```rust
validator(params: PolicyParams) { // Here you specify the additional parameters
  fn minter(tn: TokenName, ctx: ScriptContext) -> Bool { // Here the rdm and ctx.
      ...
    }
}
```

一般的なルールとして、これらの内部関数には通常、2から3個の引数があります（`datum`、`redeemer`、`script context`に対応します）。この範囲は、それが「spending」、「minting」、「publishing」、または「withdrawal」バリデーターであるかによって異なる可能性があります。

## スマートコントラクトの概要

私たちの「minter」バリデーターが行うことを考えてみましょう。**基本的に、我々は「contributor token」と同じ名前のNFTを作成することを目指していますが、プレフィックスの「222」を除外します。** 以下は、全体のプロセスを示す簡略化された図です。

![alt minter.ak](/minter-validator.png)

では、コードに移りましょう。まず、いくつかの重要な情報を抽出します。ここでは、関数型言語のクールな機能の1つである **「deconstructuring」** を使用しています。これは、後で使用できる部分に変数を分解する方法です。

```rust
// Deconstructure the the ScriptContext to get the transaction and purpose field.
let ScriptContext { transaction, purpose } = ctx
// Expect the minting script purpose and get the policy ID by deconstructuring the purpose.
expect tx.Mint(policy_id) = purpose
// Deconstructure the transaction variable to get the inputs and the minted value. 
let Transaction { inputs, mint, .. } = transaction
// Get total value from inputs.
let all_input_values: Value = inputs_value(inputs)
// Then from the previous step, get a list of policyids from the inputs.
let policies: List<PolicyId> = policies(all_input_values)
```

 では、この情報を手に入れたら、次の4つの条件をチェックできます。

1. **Policy ID** を確認して、トランザクションの入力が単一の **Contributor Token** を持っていることを検証します。

2. **鋳造された金額** が正確に **1** であることを確認します。

3. 鋳造されたトークンの`token name`が、`redeemer`で指定されたものと一致するかを検証します。

4. `redeemer`で指定された`token name`が、「PPBL2023 Token」のプレフィックス`222`を除いたものと一致することを確認します。


これらの条件が満たされると、validatorはスクリプトのpolicy IDと、次のような`token name`を持つNFTを鋳造することを許可します：`PBBL2023<あなたの名前>`。

**ビデオで詳細なステップバイステップの説明をご覧いただけます！**

## 演習：「compare」関数を完成させる

`validators/minter.ak`ファイルの最後に、未完成の関数があります。それを解決して、すべてのユニットテストをパスしてください。

```rust
// Compare the token name without the label. 
fn comparing(a: ByteArray, b: ByteArray) -> Bool {
  // todo
}
```

## 学び続ける

* スマートコントラクト設計についてさらに学びたい場合は、ドキュメントから[EUTxO Crash course](https://aiken-lang.org/fundamentals/eutxo)をチェックすることを強くお勧めします。
* このバリデーターでは、2回目またはそれ以降のトランザクションで重複したトークンが鋳造されるのを防ぐ仕組みがありません。そのため、アプリケーションを少し拡張する必要があります。そのための方法を考えられますか？
* `blueprint`と`uplc`コマンドを探索してください。最後のコマンドは、Plutus仮想マシンが読み取るスクリプトコードを表示します！




import MDXLessonLayout from "@/src/components/lms/Lesson/MDXLessonLayout.tsx";
export default ({ children }) => <MDXLessonLayout>{children}</MDXLessonLayout>;
