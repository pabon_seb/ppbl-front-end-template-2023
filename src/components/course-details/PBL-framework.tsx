import {
  Box,
  Container,
  Divider,
  Flex,
  Heading,
  Image,
  Text,
  Stack,
  Center,
  Grid,
  GridItem,
  Button,
  useColorModeValue,
  Link as CLink
} from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const PBLFramework = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue")
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size={["lg", "2xl"]} color={textColorBlue} lineHeight="1.2em">
        Project-Based Learning (PBL)コースの目的は、人々が実際のプロジェクトに貢献できるようにサポートすることです。
      </Heading>
      <Text py="5">
        <Text as="span" fontWeight="bold" color="theme.green">
          Plutus PBL 2023は、PBLコースの一例です。
        </Text>{" "}
        はPBLコースの一例です。おそらく、他にもいくつか想像がつくと思います。
      </Text>

      <Text py="5">
        Project-Based Learningの全てのコースは、一連の{" "}
        <Text as="span" fontWeight="bold" color="theme.green">
          モジュール
        </Text>
        で構成されています。
      </Text>
      <Text py="5">
        このPlutus PBLコースには14のモジュールがあります。最初の<Link href="/get-started/modules"><CLink>8つのモジュール</CLink></Link>は現在利用可能です。残りのモジュールは、12人の学生が<Link href="/modules/204/slts"><CLink>モジュール204を完了</CLink></Link>した後に公開されます。 
      </Text>
      <Container maxWidth={["100%", "90%"]} bgColor="theme.lightGray" marginTop="2em" marginLeft="0">
        <Stack>
          <Heading size="md" lineHeight="1.4" mt="1em" color="white" textAlign="center">
            モジュールは番号が振られ、PBLフレームワークで組織化されています:
          </Heading>
          <Grid templateColumns="repeat(5, 1fr)" gap={5} p="5">
            <GridItem colSpan={[5, 3]}>
              <Box>
                <Text fontWeight="bold" m="3" color="white">
                  Onboarding (100):
                </Text>

                <Text m="3" color="white">
                  それは何ですか？どうやって始めればいいですか？
                </Text>

                <Divider my="8" />

                <Text fontWeight="bold" m="3" color="white">
                  Building Background Knowledge (200):
                </Text>
                <Text m="3" color="white">
                  それはどう機能するのですか？私は何を知る必要がありますか？
                </Text>

                <Divider my="8" />

                <Text fontWeight="bold" m="3" color="white">
                  Specializing (300):
                </Text>
                <Text m="3" color="white">
                  どのように構築しましたか？
                </Text>

                <Divider my="8" />

                <Text fontWeight="bold" m="3" color="white">
                  Contributing (400):
                </Text>
                <Text m="3" color="white">
                  貢献するにはどうすればいいですか？
                </Text>
              </Box>
            </GridItem>
            <GridItem colSpan={[5, 2]}>
              <Image src="/PBLFramework.png" width="100%" alt="pblframework" />
            </GridItem>
          </Grid>
        </Stack>
      </Container>
      <Divider my="3" />
      <Heading size="xl" color={textColorBlue} py="2">
        次へ:
      </Heading>
      <Text my="5">
        各モジュールには、あなたが学ぶ内容を説明するStudent Learning Targets（SLTs）のセットが含まれています。
      </Text>
      <Link href="/get-started/slts">
        <Button>SLTsについて教えてください。</Button>
      </Link>
    </Box>
  );
};

export default PBLFramework;
