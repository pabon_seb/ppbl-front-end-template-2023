import { Button, Box, Divider, Heading, Text, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const Governance = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size="2xl" color={textColorBlue}>
        貢献を超えたものが、ガバナンスである。
      </Heading>
      <Text marginTop="1em">
        私たちの長期的な目標は、あなたが<Text as="span" fontWeight="bold" color="theme.green">いくつかのプロジェクトへの単なる貢献者以上のものになることです。</Text>
      </Text>
      <Text marginTop="1em">
        私たちはあなたに質問に答えるのを助ける意思<Text as="span" fontWeight="bold" color="theme.green">決定者</Text>になってほしいです:
      </Text>

      <Heading size="2xl" color={textColorBlue} my="10">
       「次に何をしましょうか？」
      </Heading>
      <Text fontWeight="bold" color="theme.green" my="1em">
        私たちは、コミュニティ教育が優れたガバナンスシステムの構築に不可欠であると考えています。
      </Text>
      <Text my="1em">
        私たちは、実際に物事を行うことによって最もよく学ぶと信じています。
      </Text>
      <Text my="1em">
        では始めましょう。
      </Text>
      <Link href="/modules/100">
        <Button my="10">100番のモジュールを始めましょう。</Button>
      </Link>
    </Box>
  );
};

export default Governance;

