import { Button, Box, Heading, List, ListIcon, ListItem, Text, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import ModuleListWithSLTs from "../lms/Course/ModuleListWithSLTs";

const ListOfModules = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue")
  return (
    <Box w="95%" marginTop="2em">
      <Heading size="2xl" color={textColorBlue}>
        Plutus PBLコース概要
      </Heading>
      <Text fontSize="xl" pb="5">
        コースモジュールには番号が振られています。 また、各モジュールのStudent Learning Targets（SLT）には、番号が振られています。 この番号付けシステムの目的は、学んでいることを素早く参照するための方法を提供することです。
      </Text>
      <Text fontSize="xl" pb="5">
        たとえば、SLT102.3で行き詰まった場合、チューターやクラスメートに「SLT102.3の助けが必要です」と伝えることができます。そうすることで、あなたが「cardano-cliを使ってトランザクションを構築することを学んでいる」ことがすぐに伝わります。
        PPBL 2023で学ぶ内容の概要については、以下のSLTsを参照してください。
      </Text>
      <Text fontSize="lg" fontWeight="bold" color="theme.yellow" pb="5">
        Student Learning Targetsを確認するには、モジュールをクリックしてください。
      </Text>
      <ModuleListWithSLTs />
      <Heading size="xl" color={textColorBlue} my="0.8em">
        次へ:
      </Heading>
      <Text fontSize="xl" my="5">
        Plutus Project-Based Learningのヘルプの入手方法は?
      </Text>
      <Link href="/get-started/getting-help">
        <Button>ヘルプを受けるには</Button>
      </Link>
    </Box>
  );
};

export default ListOfModules;
