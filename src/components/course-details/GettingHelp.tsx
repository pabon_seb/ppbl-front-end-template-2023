import { Button, Box, Divider, Heading, ListItem, OrderedList, Text, Link as CLink, useColorModeValue } from "@chakra-ui/react";
import Link from "next/link";
import React from "react";
import GetHelp from "../lms/Course/GetHelp";

const GettingHelp = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  return (
    <Box w="95%" marginTop="2em" fontSize="xl">
      <Heading size="2xl" color={textColorBlue}>
        行き詰まったときはどうする？
      </Heading>
      <Text py="3">
        このPlutusのProject-based Learningコースを進めていくうちに、きっと疑問が出てくるはずです。そう願っています！ 
      </Text>
      <Text py="3">
        このコースは、必要な基本情報を示し、その後、実際に何かを試してみることを目的としています。何かを試す素晴らしいことは、自分が何を知っていて、何を知らないかを発見できるということです。
      </Text>
      <Text py="3">
        時には新しいことを知ることができ、また別の時には行き詰まることがあるかもしれません。
      </Text>
      <Text py="3">
        もし行き詰まった場合は、助けを求める方法が2つあります:
      </Text>
      <OrderedList ml="10" mb="10">
        <ListItem pl="3">Gimbalabs Discord<CLink href="https://discord.gg/Va7DXqSSn8">で助けを求める。</CLink></ListItem>
        <ListItem pl="3">毎週水曜日または木曜日の<CLink><Link href="/live-coding">UTC 1430-1600に開催されるPlutus PBLライブコーディングセッションに参加する。</Link></CLink></ListItem>
      </OrderedList>
      <Divider py="3" />
      <Text py="3" fontWeight="bold" color="theme.green">
        各レッスンページの下部に以下のボタンがあるので、確認してください:
      </Text>
      <GetHelp />
      <Heading size="xl" color={textColorBlue} my="0.8em">
        次へ:
      </Heading>
      <Text fontSize="xl" my="5">
        私たちのすべての作業は、ガバナンスに向けられています。
      </Text>
      <Link href="/get-started/governance">
        <Button>ガバナンス</Button>
      </Link>
    </Box>
  );
};

export default GettingHelp;
