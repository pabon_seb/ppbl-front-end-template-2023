import {
  Box,
  Container,
  Divider,
  Heading,
  ListItem,
  OrderedList,
  Stack,
  Text,
  UnorderedList,
  Button,
  useColorModeValue,
} from "@chakra-ui/react";
import Link from "next/link";
import React from "react";

const AboutSLTs = () => {
  const textColorBlue = useColorModeValue("theme.darkBlue", "theme.blue");
  return (
    <Box w="95%" marginTop="2em">
      <Heading size={["lg", "2xl"]} color={textColorBlue}>
        Student Learning Targets（SLT）について。
      </Heading>
      <Heading size={["md", "lg"]} color="theme.green">
        このコースの仕組みはこうです:
      </Heading>
      <OrderedList marginLeft="3em">
        <ListItem fontSize="xl" py="2">
          PPBL 2023は14のコースモジュールで構成されています。
        </ListItem>
        <ListItem fontSize="xl" py="2">
          各モジュールは、Student Learning Targets (SLTs)のリストで始まります。
        </ListItem>
        <ListItem fontSize="xl" py="2">
          各モジュールのレッスンは、各Learning Student Target (SLT)をマスターできるように設計されています。
        </ListItem>
        <ListItem fontSize="xl" py="2">
          Cardanoがどのように機能するかを体験的に学ぶために、数個のプロジェクトを完了する必要があります。
        </ListItem>
        <ListItem fontSize="xl" py="2">
          その過程で、SLTの達人ぶりを発揮することになります。
        </ListItem>
        <ListItem fontSize="xl" py="2">
          一緒に、CardanoとPlutusを使用して、あなたが学んだことの記録を共有する方法を探求します。
        </ListItem>
      </OrderedList>
      <Divider py="5" />
      <Heading size="xl" color={textColorBlue}>
        Student Learning Target とは？
      </Heading>
      <Text fontSize="xl" mb="5">
        Student Learning Target (SLT)は、各モジュールの終了時にあなたが知っていること、またはできるようになることを説明するものです。
      </Text>
      <Text fontSize="xl" my="5">
        SLTは、私たちが各レッスンを作成した理由を理解するのに役立つはずです。
      </Text>
      <Divider py="5" />
      <Heading size="xl" color={textColorBlue}>
        次へ:
      </Heading>
      <Text fontSize="xl" mb="5">
        PPBL 2023のコースモジュールとStudent Learning Targetsのリストをご覧ください。
      </Text>
      <Link href="/get-started/modules">
        <Button>モジュールとSLTを表示してください</Button>
      </Link>
    </Box>
  );
};

export default AboutSLTs;