import {
  Center,
  Flex,
  Box,
  Text,
  Heading,
  Grid,
  GridItem,
  Divider,
  Button,
  Spacer,
  Link as CLink,
} from "@chakra-ui/react";
import Link from "next/link";

const GetHelp = () => {
  return (
    <Flex direction={["column", "row"]} p="3">
      <Spacer />

      <Heading size="lg" textAlign="center">
        お困りですか？
      </Heading>
      <Spacer />
      <Flex direction="column" justifyItems="center">
        <Text fontSize="xl" textAlign="center">
          Discordで質問してください
        </Text>
        <CLink href="https://discord.gg/Va7DXqSSn8" target="_blank">
          <Box mt="2" bg="theme.yellow" borderRadius="lg" _hover={{ bg: "theme.blue" }}>
            <Text textAlign="center" color="theme.dark">
              GimbalabsのDiscordに参加してください
            </Text>
          </Box>
        </CLink>
      </Flex>
      <Spacer />
      <Flex direction="column" justifyItems="center">
        <Text fontSize="xl" textAlign="center">
          Plutus PBLライブコーディングに参加してください
        </Text>
        <Link href="/live-coding">
          <Box mt="2" bg="theme.yellow" color="theme.dark" borderRadius="lg" _hover={{ bg: "theme.blue" }}>
            <Text textAlign="center">ライブコーディングのカレンダーを見る</Text>
          </Box>
        </Link>
      </Flex>
      <Spacer />

    </Flex>
  );
};

export default GetHelp;
