import { Box, Text, Heading, Center, Badge, Flex, Spacer, Divider } from "@chakra-ui/react";
import * as React from "react";

type Props = {
  children?: React.ReactNode;
  mastery: boolean;
};
const SuccessComponent: React.FC<Props> = ({ children, mastery }) => {
  return (
    <Box my="3" py="3" bg="theme.lightGray" color="white">
      <Center>
        <Heading size="md">「マスタリー」ステータス</Heading>
        {mastery ? (
          <Badge ml="5" fontSize="lg" bg="theme.green">
            <Text size="md">成功!</Text>
          </Badge>
        ) : (
          <Badge ml="5" fontSize="lg" bg="theme.yellow" color="theme.dark">
            <Text size="md">進行中</Text>
          </Badge>
        )}
      </Center>
      <Center>
        <Box fontSize="lg" py="3">
          {children}
        </Box>
      </Center>
    </Box>
  );
};

export default SuccessComponent;
