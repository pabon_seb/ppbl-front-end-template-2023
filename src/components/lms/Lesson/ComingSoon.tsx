import { Container, Divider, Heading, Box } from "@chakra-ui/react";

const ComingSoon = () => {
  return (
    <Container maxWidth="90%" marginTop="2em">
      <Box py="5">
        <Heading>近日公開予定</Heading>
      </Box>
    </Container>
  );
};

export default ComingSoon;