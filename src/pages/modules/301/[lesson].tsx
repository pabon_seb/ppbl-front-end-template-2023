import SLTs301 from "@/src/components/course-modules/301/slts";
import ModuleLessons from "@/src/components/lms/Lesson/Lesson";
import ComingSoon from "@/src/components/lms/Lesson/ComingSoon"
import slt from "@/src/data/slts-japanese.json"
import Lesson3011 from "@/src/components/course-modules/301/3011";
import Lesson3012 from "@/src/components/course-modules/301/3012";
import Lesson3013 from "@/src/components/course-modules/301/3013";
import Lesson3014 from "@/src/components/course-modules/301/3014";
import Assignment301 from "@/src/components/course-modules/301/assignment301";

const Module301Lessons = () => {

  const moduleSelected = slt.modules.find((m) => m.number === 301);

  const status = null

  const lessons = [
    { key:"slts", component:<SLTs301 />},
    { key:"3011", component:<Lesson3011 />},
    { key:"3012", component:<Lesson3012 />},
    { key:"3013", component:<Lesson3013 />},
    { key:"3014", component:<Lesson3014 />},
    { key:"assignment301", component:<Assignment301 />},
  ]

  return (
    <ModuleLessons items={moduleSelected?.lessons ?? []} modulePath="/modules/301" selected={0} lessons={lessons} status={status}/>
  )

};

export default Module301Lessons;