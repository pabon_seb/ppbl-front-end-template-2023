import Head from "next/head";
import { Box, Center, Divider, Heading, Link as CLink, Text } from "@chakra-ui/react";
import { useContext } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";
import { hexToString } from "../utils";
import Link from "next/link";
import StudentMasteryByModule from "../components/lms/Mastery/StudentMasteryByModule";

// Add List of Mastery Assignments

export default function StudentDashboardPage() {
  const ppblContext = useContext(PPBLContext);

  return (
    <>
      <Head>
        <title>PPBL 2023</title>
        <meta name="description" content="Plutus Project-Based Learning from Gimbalabs" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={["100%", "70%"]} mx="auto" my="10">
        <Heading>生徒用ページ</Heading>
        <Text py="3">ブラウザウォレットを接続して、このコースで作成したいくつかのon-chainデータを表示してください。</Text>
        <Divider />
        <Heading color="theme.yellow">
          Your Connected Contributor Token:
        </Heading>
        <pre>{JSON.stringify(ppblContext.connectedContribToken, null, 2)}</pre>
        <Heading color="theme.yellow">
          Your Student Reference Datum:
        </Heading>
        {ppblContext.contributorReferenceDatum ? (
          <>
            <Box>
              <Text fontSize="4xl">Your lucky number is {ppblContext.contributorReferenceDatum?.fields[0].int}</Text>
              <StudentMasteryByModule />
            </Box>
          </>
        ) : (
          <Text>No Datum found. PPBL 2023 tokenを保持しているブラウザウォレットを接続してください。</Text>
        )}
        <Heading color="theme.yellow">
          Your CLI Address:
        </Heading>
        <pre>{JSON.stringify(ppblContext.cliAddress, null, 2)}</pre>
        <Divider mt="5" />
        <Heading>モジュール203で予定されている内容は次の通りです。</Heading>
        <Text>
          このデータについて詳細に見ていきます。現在のCLI addressの取得方法は十分ですか？datumには、他に何を追加したいですか？{" "}
          <CLink href="https://gitlab.com/gimbalabs/ppbl-2023/ppbl-2023-token-registry">Token Registry</CLink>において、JSONファイルをどのように活用できるでしょうか？
        </Text>
      </Box>
    </>
  );
}
