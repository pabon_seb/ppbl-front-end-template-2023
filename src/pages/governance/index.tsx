import { Heading, Box, Link as CLink, Text, Divider, List, ListItem, OrderedList } from "@chakra-ui/react";
import Head from "next/head";

export default function GovernancePage() {
  return (
    <>
      <Head>
        <title>Gimbalabs PBL Governance</title>
        <meta name="description" content="About Plutus Project-Based Learning" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box w={{ base: "95%", md: "85%", lg: "75%" }} mx="auto">
        <Heading py="10" size="2xl">
          Gimbalabs PBL Governance Sessions
        </Heading>
        <Text fontSize="xl" py="3">
          2024年1月と2月、Plutus PBL Live CodingはGimbalabs PBL Governance Sessionsに置き換えられます。
        </Text>
        <Text fontSize="xl" py="3">
          PBL Governance Sessionsは、水曜日と木曜日のUTC 1430から1600に、Live Codingと同じ時間帯で開催されます。
        </Text>
        <Heading py="5" mt="5" borderTop="1px">
          導く質問：
        </Heading>
        <OrderedList fontSize="xl">
          <ListItem py="2" pl="3" ml="3">
            Plutus PBLの次の展開は何ですか？
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            Gimbalabsの次の展開は何ですか？
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            他にも優先すべき「project-based learning」コースはありますか？
          </ListItem>
        </OrderedList>
        <Heading py="5" mt="5" borderTop="1px">
          参加すべき対象は誰ですか？
        </Heading>
        <Text fontSize="xl" py="3">
          誰もが歓迎されています。 特に、もし以下の条件に当てはまる場合は参加することをお勧めします:
        </Text>
        <OrderedList fontSize="xl">
          <ListItem py="2" pl="3" ml="3">
            Plutus PBL 2023に参加された方で、Gimbalabsで貢献したい、または進行中のプロジェクトに関わりたいとお考えの方は。
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            Gimbalabsの2024年の優先事項を決定するお手伝いをしたい場合は、以下の通りです。
          </ListItem>
          <ListItem py="2" pl="3" ml="3">
            <CLink href="https://sociocracy30.org">Sociocracy 3.0</CLink>の実践に協力したい場合は、以下の通りです。
          </ListItem>
        </OrderedList>
        <Heading py="5" mt="5" borderTop="1px">
          登録：
        </Heading>
        <Text fontSize="xl" py="3">
          ミーティングはZoomで行われます。登録は{" "}
          <CLink href="https://us06web.zoom.us/meeting/register/tZErceCqpzMtG9XldfuPnBQEus5MBivl9OZe">こちらをクリック</CLink>
          してください。
        </Text>
        <Text fontSize="xl" py="3">
          ミーティングは、Live Codingと同じZoomリンクで行われます。{" "}
          <CLink href="https://gimbalabs.com/calendar">Gimbalabs Calendar</CLink>,を購読している場合は、何も変更する必要はありません。
        </Text>
        <Heading py="5" mt="5" borderTop="1px">
          今後のセッション：
        </Heading>

        <Text fontSize="xl" py="3">
          #1 - Driver Mapping Workshop (Part 1): 2024年1月10日（水曜日）UTC 14:30に予定されています。
        </Text>
        <Text fontSize="xl" py="3">
          #2 - Driver Mapping Workshop (Part 2): 2024年1月11日（木曜日）UTC 14:30に予定されています。
        </Text>
        {/* tell ODIN Discord! */}
        <Text fontSize="xl" py="10">
          お知らせは<CLink href="'https://discord.gg/Va7DXqSSn8">GimbalabsのDiscord</CLink>でご覧いただけます。そちらに参加してください。
        </Text>
      </Box>
    </>
  );
}

// https://us06web.zoom.us/meeting/register/tZErceCqpzMtG9XldfuPnBQEus5MBivl9OZe
