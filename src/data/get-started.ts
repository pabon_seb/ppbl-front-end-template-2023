export const items = [
  {
    slug: "framework",
    name: "PBLフレームワーク",
  },
  {
    slug: "slts",
    name: "Student Learning Targetsについて",
  },
  {
    slug: "modules",
    name: "コースモジュールのリスト",
  },
  {
    slug: "getting-help",
    name: "ヘルプを得る方法",
  },
  {
    slug: "governance",
    name: "ガバナンス",
  },
];
